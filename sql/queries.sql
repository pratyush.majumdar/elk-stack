CREATE TABLE `contacts` (
  `uid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(80) NOT NULL,
  `first_name` varchar(80) NOT NULL,
  `last_name` varchar(80) NOT NULL,
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `contacts` (uid, email, first_name, last_name) values (NULL, 'jim@example.com', 'Jim', 'Smith');

insert into `contacts` (uid, email, first_name, last_name) values (NULL, 'john@example.com', 'John', 'Smith');
